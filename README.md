# FEVO Note
This is a homework assignment that I did when applying for job at Jumpshot.
I received a small application with functionality as described later in this readme,
created everything that can be found in the src/test folder as well as wrote the `TestBreakdown.md` document which contains a brief overview of my approach.


# Salaries

A company "BigHR" is a HR company which tries to find candidates to hire. Besides the primary focus it tries to gain
some information about available positions, like average salary per position, age, gender and year.
The format is as follows:

```
| year | gender | age | position | salary |
```

On the end of each year, the data are processed - aggregated, to be able to see average salary:

- per year, position, gender
- per year, position, age   

For the aggregation purposes the company developed dedicated application.

# Your task

Write automated tests for the application. Cucumber test style is preferable, particular framework and underlying language is up to your choice.
You don't have to fix the application itself if you find any bug.

The application can be built with:

```
./gradlew build
```

The application can be run with:

```
java -jar build/libs/bighr.jar
```

**NOTE:** The input data are already contained within the application (`src/main/resources/salaries.tsv`).


The results are printed on standard output. 
