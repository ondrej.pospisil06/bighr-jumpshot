## General info
Tables below  show basic breakdown of functionality across the application.
It should serve as brief overview of test coverage as well as help during writing up test cases
to see what has been already covered and on what level. <br>

Tests are divided into two categories - Unit-style and Acceptance. Both are using ScalaTest as a testing framework of choice with few differences in used styles. <br>
 
Unit-style test cases are trying to cover each function within the application more from the code point of view. 
Using FlatSpec, Assertions and some MustMatchers to reflect a bit BDD style.

Acceptance tests are writen in more BDD style with FeatureSpec and Given/When/Then. Trying to look at the app from users point of view.
Defining basic use-cases and flow through the application. 

| Unit-style_tests |                    |                           |                   |                                                         |
|:-----------------|:-------------------|:--------------------------|:------------------|:--------------------------------------------------------|
|                  | **test_target_1**  | **test_target_2**         | **test_target_3** | **expected_result**                                     |
| RowAverage.scala | .toString override |                           |                   | Properly formatted string from input                    |
| Runner.scala     | .loadSalaries      | empty input file          |                   | Exception thrown: (tail of empty stream)                |
|                  |                    | only headers in file      |                   | Provides empty collection                               |
|                  |                    | correct records from file |                   | Creates RowSalary collection with correct data and size |
|                  |                    | incorrect data in cell    |                   | Exception thrown: (NumberFormat)                        |
|                  |                    | more columns in source    |                   | Only valid columns will be mapped                       |

| Acceptance_tests |                   |                                |                   |                                                          |
|:-----------------|:------------------|:-------------------------------|:------------------|:---------------------------------------------------------|
|                  | **test_target_1** | **test_target_2**              | **test_target_3** | **expected_result**                                      |
| AppDataAggTest   | loading data      | valid file is provided         |                   | Data gets through and output is provided                 |
|                  |                   | file is completely empty       |                   | User encounters an exception (tail of empty stream)      |
|                  |                   | file is missing                |                   | User encounters an exception (NullPointer)               |
|                  | data aggregation  | input contains only one record |                   | Data are still aggregated correctly                      |
|                  |                   | input contains no data rows    |                   | Provides empty output                                    |
|                  |                   | valid data aggregation         |                   | Output values are correct for complex input              |


## Running the tests
Unit tests can be run with gradle task unitTest
```
./gradlew unitTest
```
Acceptance tests then with task acceptanceTest
```
./gradlew acceptanceTest
```
All tests can be run with basic test task
```
./gradlew test
```


## NOTES:
### Description of desired output is a bit misleading compared to the output provided by the application <br>
```
On the end of each year, the data are processed - aggregated, to be able to see average salary:
per year, position, gender
per year, position, age
```
this would lead me to believe that for every year I will get output in format something like: <br>
\- data aggregated by gender: <br>
\[YEAR] - \[position] - \[M] - \[*no age*] - \[AVG salary regardless of age] <br>
\[YEAR] - \[position] - \[F] - \[*no age*] - \[AVG salary regardless of age] <br>
...
 

\- data aggregated by age: <br>
\[YEAR] - \[position] - \[*no gender*]- \[AGE1] - \[AVG salary of all employees of the same age on the same position regardless of gender] <br>
\[YEAR] - \[position] - \[*no gender*]- \[..AGE_X]- \[AVG salary of all employees of the same age on the same position regardless of gender]<br>
...

For this exercise I'm going with the option that the description is a bit unfortunately worded and the implementation thus 
does not provide the output the customer wanted.
Due to this, the BDD style Acceptance tests will be mostly failing as for the sample input data the output is invalid. 

As for testing the output. There is a lot that could be specified about whether and how the output should be formatted/sorted/grouped. So for 
this I'm creating just an example test case of the "desired" order of results.
 

### Testability of the application itself
* In the Runner there was a very impure function, that was supposed to load data from a TSV file. It did not took any parameters
and the file path was hard-coded to the body of the function. <br>
The only way how to test this function would be to change content of the TSV file for each test case. This would lead to quite 
an overhead in some form of BeforeAndAfter functions/gradle tasks/bash scripts that would prepare the desired TSV. <br>
For purpose of this exercise I took the liberty of changing the function to take one parameter of type Source. This can then be
provided in various forms (strings, files, streams etc.) allowing for mocking the source data by simple strings in memory and 
preserving the original functionality at the same time.
 
* On the note of the data loading function alone. It could definitely use some polishing. As it is now it is still impure function
that can throw a lot of different exceptions (as is the usual case when working with external sources and events) but it can definitely
be made more safe and concise. Probably take advantage of Scala's Option/Some/None, make some data validation and error handling before
returning values. 
So in this case the basic test scenarios around data loading are verifying the Exceptions thrown as a correct behavior. But in real life application
this should not be the case and this whole matter should be approached differently.


### side note:
Also given the fact the the app currently does not provide the output as per description, I went ahead and created a separate branch in this repository
called data-agg-fix.
In this branch I did some small additions to the code, that enables the functionality as I think it was originally intended and which is considered as expected behavior in my tests.

- `RowAverage.scala` <br>
Added parameters gender and age as optional with some default values. This enables the aggregation function to leave out gender or age depending on which aggregation is being calculated.<br>
Added small matcher that helps format the output in case age is not used (aggregation by gender)
- `AverageSalaries.process` <br>
Added a bit of a hack by duplicating the .groupBy - .map - .toSeq sequence and joining the two collections to one before returning.
This is because the first sequence is leaving out AGE from the grouping filter and correctly aggregating data only by year, position, gender
The later sequence is doing exactly the opposite. Leaving out GENDER from the .groupBy and thus aggregating by year, position, age
- `Runner.scala` <br>
Added a small function that is calling the data aggregation function. Just for better testability.

With these changes, all the tests in AppDataAggTest class should now be passing as the application is working "according to the spec"



