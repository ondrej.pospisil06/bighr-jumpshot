package com.jumpshot.acceptance

import com.jumpshot.bighr.Runner.loadSalaries
import com.jumpshot.bighr.{AverageSalaries, Runner}
import org.scalatest.{FeatureSpec, GivenWhenThen, MustMatchers}

import scala.io.{BufferedSource, Source}


class AppDataAggTest extends FeatureSpec with GivenWhenThen with MustMatchers {

  info("As a recruiter")
  info("I want to be able to obtain historical average salary overview")
  info("To be able to better target candidates and monitor trends")

  feature("App loads up sources correctly and handles errors") {

    scenario("User loads a correct source data file") {
      Given("Source file is present on the correct path and is not empty")
      val tsvSrc: BufferedSource = Source.fromResource("salaries.tsv")
      assert(tsvSrc.nonEmpty)

      When("File is loaded and parsed")
      val parsedData = Runner.loadSalaries(tsvSrc)
      assert(parsedData.nonEmpty)

      Then("App aggregates data and prints them to standard output")
      val calculated = AverageSalaries.process(parsedData)
      assert(calculated.nonEmpty)
      //TODO possible STDOUT verification
    }

    scenario("User loads completely empty file") {
      Given("Path to file is correct and file is empty")
      val srcData = Source.fromResource("empty.tsv")

      When("App tries to parse the file")
      val thrown = intercept[Exception]( Runner.loadSalaries(srcData))

      /* This is a bad approach and Exceptions (especially NPE in Scala)
      should not be thrown at user for such trivial tasks.
      Generally an error handling should be in place that would inform the user
      of invalid input and exit the program gracefully.
       */
      Then("User is presented with an Exception (tail of empty stream)")
      assert(thrown.getMessage.contains("tail of empty stream"))
    }

    scenario("User attempts to load non-existent file") {
      Given("Path to file is incorrect")
      val srcData = Source.fromResource("totally_present_file.tsv")

      When("App tries to load empty file")
      val thrown = intercept[Exception]( Runner.loadSalaries(srcData))

      Then("User is presented with an Exception (NullPointerException)")
      assert(thrown.toString.contains("NullPointerException"))
    }

  }

  /* As described briefly in the test breakdown document.
  These scenarios mostly fail for the original application.
  This is due to the fact, that the original code incorrectly includes
  age and gender in all of the grouping instead of making it once aggregated
  by gender and once by age.
   */
  feature("App aggregates data and shows correct outputs for given inputs") {

    scenario("Input is correct but contains only one record") {
      Given("Source is valid and contains only one record")
      val srcData = Source.fromString(
        """year	gender	age	position	salary
          |2017	female	21	worker	2050""".stripMargin
      )

      When("Data are aggregated")
      val aggData = AverageSalaries.process(loadSalaries(srcData)).mkString("\n")

      Then("Output values should be the same as input")
      val expData =
        """2017	worker	_	21	2050.0
          |2017	worker	female	_	2050.0""".stripMargin
      aggData.mkString("\n") must equal(expData)
    }

    scenario("App runs with correct input file that is empty") {
      Given("Source file path is correct, but file contains only headers and no data")
      val srcData = Source.fromResource("headers_only.tsv")

      When("Data are aggregated")
      val aggData = AverageSalaries.process(loadSalaries(srcData)).mkString("\n")

      Then("Output should be empty")
      assert(aggData.isEmpty)
    }

    scenario("Aggregation is correct when provided more complex data") {
      Given("Valid source file")
      val srcData = Source.fromResource("test_salaries.tsv")

      When("Data are aggregated")
      val aggData = AverageSalaries.process(loadSalaries(srcData))

      Then("They are correctly reflecting the input")
      val expData = Source.fromResource("expected_data.tsv").mkString
      aggData must equal(expData)
    }

  }

}
