package com.jumpshot.unit


import com.jumpshot.bighr.Runner
import com.jumpshot.bighr.model.RowSalary
import org.scalatest.{BeforeAndAfter, FlatSpec, MustMatchers}

import scala.io.{BufferedSource, Source}


class DataLoadTest extends FlatSpec with MustMatchers with BeforeAndAfter{

  "Runner.loadSalaries " must "throw 'tail of empty stream' exception on empty file" in {
    val data: BufferedSource = Source.fromResource("empty.tsv")

    val thrown = intercept[Exception]( Runner.loadSalaries(data))
    assert(thrown.getMessage.contentEquals("tail of empty stream"))
  }

  it must "Provide empty collection when only headers are in file" in {
    val data: BufferedSource = Source.fromResource("headers_only.tsv")

    val loaded = Runner.loadSalaries(data)
    loaded.size must be (0)
  }

  it must "create valid RowSalary collection (both data and size)" in {
    val data = Source.fromString(
      """year	gender	age	position	salary
        |2017	male	23	corp	3300
        |2017	male	28	gunman	3000
        |2016	male	28	gunman	3000
        |2015	male	28	gunman	3000
        |2014	male	28	gunman	3000""".stripMargin
    )
    val rows: Seq[RowSalary] = Runner.loadSalaries(data)

    rows.head.year must be (2017)
    rows.head.gender must be ("male")
    rows.head.age must be (23)
    rows.head.position must be ("corp")
    rows.head.salary must be (3300)
    rows.size must be (5)
  }

  it must "throw NumberFormat exception when provided invalid data for numbers" in {
    val data = Source.fromString(
      """year	gender	age	position	salary
        |2017	male	23	corp	3300bla
      """.stripMargin
    )
    val thrown = intercept[Exception](Runner.loadSalaries(data))
    assert(thrown.getMessage.contentEquals("For input string: \"3300bla\""))
  }

  it must "omit unknown columns" in {
    val data = Source.fromString(
      """year	gender	age	position	salary	year	testcol2
        |2018	male	28	gunman	3000	2008	testcol2Vals
        |2017	male	28	gunman	3000	testcol1vals	testcol2Vals
        |2016	male	28	gunman	3000	testcol1vals	testcol2Vals
        |2015	male	28	gunman	3000	testcol1vals	testcol2Vals
        |2014	male	28	gunman	3000	testcol1vals	testcol2Vals""".stripMargin
    )

    val record = Runner.loadSalaries(data).head
    record.year must be(2018)
    record.gender must be ("male")
    record.age must be (28)
    record.position must be ("gunman")
    record.salary must be (3000)
  }

}
