package com.jumpshot.unit

import com.jumpshot.bighr.model.RowAverage
import org.scalatest.{FlatSpec, MustMatchers}

class RowAverageTest extends FlatSpec with MustMatchers {

  "RowAverage.toString" must "return correct, tab-spaced string" in {
    val row: RowAverage = RowAverage(2147483647, "Generaloberst im Range eines Generalfeldmarschalls", "M", 33, 5000.2147483647)
    row.toString must be ("2147483647\tGeneraloberst im Range eines Generalfeldmarschalls\tM\t33\t5000.2147483647")
  }

}
