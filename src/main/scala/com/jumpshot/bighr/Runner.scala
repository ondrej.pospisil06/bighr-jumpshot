package com.jumpshot.bighr

import com.jumpshot.bighr.model.RowSalary
import scala.io.{BufferedSource, Source}

object Runner extends App {

  val tsvSrc: BufferedSource = Source.fromResource("salaries.tsv")

  def loadSalaries(source: Source): Seq[RowSalary] = {
    source.getLines().toSeq.tail.map(_.split("\t")).map {
      fields =>
        val year :: gender :: age :: position :: salary :: _ = fields.toList
        RowSalary(year.toInt, gender, age.toInt, position, salary.toInt)
    }
  }

  println(AverageSalaries.process(loadSalaries(tsvSrc)).mkString("\n"))
}