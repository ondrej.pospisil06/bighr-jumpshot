package com.jumpshot.bighr.model

case class RowSalary(year: Int, gender: String, age: Int, position: String, salary: Int)
