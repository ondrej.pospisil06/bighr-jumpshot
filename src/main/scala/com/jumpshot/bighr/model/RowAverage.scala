package com.jumpshot.bighr.model

case class RowAverage(year: Int, position: String, gender: String, age: Int, averageSalary: Double) {
  override def toString = s"$year\t$position\t$gender\t$age\t$averageSalary"
}
