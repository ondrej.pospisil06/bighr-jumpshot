package com.jumpshot.bighr

import com.jumpshot.bighr.model.{RowAverage, RowSalary}

object AverageSalaries {

  def process(data: Seq[RowSalary]): Seq[RowAverage] = {
    data
      .groupBy(r => (r.year, r.position, r.gender, r.age))
      .mapValues(rs => {
        val salaries = rs.map(_.salary)
        salaries.sum / salaries.size
      })
      .map {
        case ((year, position, gender, age), averageSalary) =>
          RowAverage(year, position, gender, age, averageSalary)
      }.toSeq
  }
}
